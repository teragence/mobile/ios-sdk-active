# ios-teragence-sdk-active

[![Version](https://img.shields.io/cocoapods/v/ios-teragence-sdk-active.svg?style=flat)](http://cocoapods.org/pods/ios-teragence-sdk-active)
[![License](https://img.shields.io/cocoapods/l/ios-teragence-sdk-active.svg?style=flat)](http://cocoapods.org/pods/ios-teragence-sdk-active)
[![Platform](https://img.shields.io/cocoapods/p/ios-teragence-sdk-active.svg?style=flat)](http://cocoapods.org/pods/ios-teragence-sdk-active)

# Requirements

## Installation and Integration guide

 1) **ios-teragence-sdk-active is available** through [CocoaPods](http://cocoapods.org). To install it, simply add the following line to your Podfile:

```ruby
pod 'ios-teragence-sdk-active'
```

 2) Open the `AppDelegate.m` and import the framework:

````
#import <SDK_Teragence/SDK_Teragence.h>
````

 3) Then add in interface following property:

````
@interface AppDelegate ()
@property (nonatomic, strong) TRGController *sdk;
@end
````

 4) In the method `didFinishLaunchingWithOptions` of the `AppDelegate.m` add the following code: 

````
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
// Your code
self.sdk = [[TRGController alloc] initWithPartnerID:@"PartnerID"];

return YES;
}
````
where :
> PartnerID
**It's your unique Partner ID symbols**

**framework** is a directory that contains a Unix executable file, public headers, info.plist and LLVM Module Map.

 5) Allowed controls `TRGController`

[x] Allows conduct one-time measurements when the application is running by calling 
> (void)makeManualMesurements;

method.


## Project setup

 1) Add the `NSLocationWhenInUseUsageDescription` key and the `NSLocationAlwaysAndWhenInUseUsageDescription` key to your Info.plist file. (Xcode displays these keys as `"Privacy - Location When In Use Usage Description"` and `"Privacy - Location Always and When In Use Usage Description"` in the Info.plist editor.)

Сode for manual editing of Info.plist:

````
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
<string>For detecting of your location.</string>
<key>NSLocationAlwaysUsageDescription</key>
<string>For detecting of your location.</string>
````

 2) If your app supports iOS 10 and earlier, add the `NSLocationAlwaysUsageDescription` key to your Info.plist file. (Xcode displays this key as `"Privacy - Location Always Usage Description"` in the Info.plist editor.)

Сode for manual editing of Info.plist:

````
<key>NSLocationAlwaysUsageDescription</key>
<string>For detecting of your location.</string>
````

 3) For each key you must write a convincing description of why you need to receive location updates in the background. For example - `“For detecting of your location”`.

 4) Select the “Capabilities” tab for your app target and in “Background Modes” select “Location updates”.

 5) Add the `“Required Background Modes”` key with value `“App registers for location updates”` to your info plist file.

Сode for manual editing of Info.plist:

````
<key>UIBackgroundModes</key>
<array>
<string>location</string>
</array>
````

 6) Add the `“App Transport Security Settings” - “Allow Arbitrary Loads”` key with value `“YES”` to your info plist file. 

Or open Info.plist as source code and insert this:

````
<key>NSAppTransportSecurity</key>
<dict>
<key>NSAllowsArbitraryLoads</key>
<true/>
</dict>
````

## Author

>Teragence  info@teragence.com

## License

**ios-teragence-sdk-active** is available under the **EULA** license. See the [LICENSE](https://bitbucket.org/teragence/ios-sdk-active/src/012fc218772519bf4508c6ed2a5f8e772e695c0a/LICENSE?at=master&fileviewer=file-view-default)  file for more info.

## Release notes



